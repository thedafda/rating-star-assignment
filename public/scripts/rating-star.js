(function(w){

	var colors = [
		"red",
		"green",
		"orange",
		"white",
		"black"
	];

	var noop = function(){};

	function RatingStar(ele, options){
		options = options || {};
		this.ele = ele;
		this.name = options.name ||  ele.getAttribute('rating-star') || 'Star';
		this.colorIndex = options.colorIndex || ele.getAttribute('star-initial-val') || 0;
		this.onChange = options.onChange || noop;
		this.render();
		this.bindEvents();
	}

	RatingStar.prototype.render = function() {
		this.ele.innerHTML = [this.name, '<span>&#9733;</span>'].join(' ');
		this.starEle = this.ele.querySelector('span');
		this.starEle.style.color = colors[this.colorIndex];
	};

	RatingStar.prototype.updateColor = function(){
		this.colorIndex++;
		if(this.colorIndex >= colors.length){
			this.colorIndex = 0;
		}
	};

	RatingStar.prototype.bindEvents = function() {
		var self = this;
		this.events = this.events || {};
		this.events.click = function(){
			self.updateColor();
			self.starEle.style.color = colors[self.colorIndex];
            self.onChange && self.onChange(self.colorIndex, this);
		};
		this.ele.addEventListener('click', this.events.click);
	};

	RatingStar.prototype.removeEvents = function() {
		this.ele.removeEventListener('click', this.events.click);
		this.innerHTML = '';
	};

	RatingStar.prototype.destroy = function() {
		this.removeEvents();
	};

	w.UI  = w.UI || {};
	w.UI.RatingStar = RatingStar;
	w.UI.initiateRatingStart = function(){
        function createStar(node, status){
            var name = node.getAttribute('rating-star');
            return new UI.RatingStar(node, {
                colorIndex: status,
                onChange: function(status){
                    $.post('../api/rating', {
                        name: name,
                        status: status
                    });
                }
            });
        }

		var allStarEles = document.querySelectorAll('[rating-star]');
		Array.prototype.map.call(allStarEles, function(starNode){
            var name = starNode.getAttribute('rating-star');
			$.get('../api/rating', {
				name: name
			}).then(function(obj){
                createStar(starNode, obj.status);
			}, function(err){
                if(err.status == 404){
                    $.post('../api/rating', {
                        name: name,
                        status: 0
                    }).then(function(obj){
                        createStar(starNode, obj.status);
                    });
                }
            });
		});
	};
}(window));