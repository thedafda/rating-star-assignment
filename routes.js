module.exports = function (app, passport) {
	var data = require('./app/models/data');

    app.get('/api/rating', function (req, res) {
        var name = req && req.query && req.query.name;
        if (name) {
            data.findOne({
                name: name
            }, function (err, docs) {
                if (err || !docs) {
                    res.status(404).send({
                        error: 'No Rating found for : ' + name
                    });
                    return;
                }
                res.json(docs);
            });
            return;
        }
        res.status(500).send({ error: 'Invalid Rating Name' });

    });

	app.post('/api/rating', function (req, res) {
        var name = req && req.body && req.body.name;
        var status = req && req.body && req.body.status;
        if(name){
            var query = {
                name: name
            };
            var updateValue = {
                name: name,
                status: status || 0
            };
            data.update(query, { $set : updateValue },{
                upsert : true
            }, function(err, docs){
                if(err){
                    res.status(500).send({ error: 'Error While updating Value' });
                    return;
                }
                res.json(updateValue);
            });
            return;
        }
        res.status(500).send({ error: 'Error While updating Value' });

	});
};