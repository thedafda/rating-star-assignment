var mongoose = require('mongoose');

module.exports = mongoose.model('userModel', {
	status: Number,
	name: String
});