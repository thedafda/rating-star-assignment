# Star App

A Node app built with MongoDB . 

Node provides the RESTful API.  MongoDB stores status and color.

## Requirements
node.js
mongodb
npm

## Installationr
1. Install Mongodb and start the mongo
2. Install the application: `npm install`
3. Start the server: `node server.js`
4. View in browser at `http://localhost:8080/public`



Description :

1. App contains the db schema model and routing the Request like delete ,update,get

2. server file contains the express and middleware,database connection 

3. config file conatins the database URL for mongoose connection